-- Criação da tabela de Estados
create table if not exists state (
	`id` bigint(11) UNSIGNED not null auto_increment,
    `name` varchar(80) not null,
    `acronym` char(2) not null, 
    `date_create` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,  
    `date_last_update` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    
    primary key(`id`)
) ENGINE=InnoDB;

-- Criação da tabela de Cidades
create table if not exists city (
	`id` bigint(11) UNSIGNED not null auto_increment,
    `name` varchar(100) not null,
    `state_id` bigint(11) UNSIGNED NOT NULL,
    `date_create` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,  
    `date_last_update` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    
    primary key(`id`),
    CONSTRAINT `fk_city_on_state_id`
    FOREIGN KEY (`state_id`)
    REFERENCES `state` (`id`)
) ENGINE=InnoDB;

-- Criação da tabela de Endereços
create table if not exists `address` (
  `id` BIGINT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` TINYINT(1) NOT NULL DEFAULT 0 COMMENT '0 = Residencial\n 1 = Comercial\n 2= Cobrança\n 3 = Entrega',
  `zip_code` VARCHAR(10) NOT NULL,
  `street` VARCHAR(150) NOT NULL,
  `number` VARCHAR(30) NULL,
  `district` VARCHAR(150) NULL,
  `complement` VARCHAR(150) NULL,
  `note` VARCHAR(150) NULL,
  `city_id` BIGINT(11) UNSIGNED  NOT NULL,
  `date_create` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,  
  `date_last_update` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  
  PRIMARY KEY (`id`),
  INDEX `idx_address_on_city_id` (`city_id` ASC),
  CONSTRAINT `fk_address_on_city_id`
    FOREIGN KEY (`city_id`)
    REFERENCES `city` (`id`)
) ENGINE = InnoDB;

-- Criação da tabela de Pessoa
create table if not exists `person` (
  `id` BIGINT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` TINYINT(1) NOT NULL DEFAULT 0 COMMENT '0 = Administrador\n 1 = Artista\n 2= Cliente\n',
  `name` VARCHAR(255) NOT NULL,
  `cpf` VARCHAR(15) NULL,
  `email` VARCHAR(50) NOT NULL,
  `phone` VARCHAR(16) NOT NULL,
  `observation` VARCHAR(255) NULL,
  `date_create` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,  
  `date_last_update` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  
  PRIMARY KEY (`id`)
) ENGINE = InnoDB;

-- Criação da tabela de Pessoa e Endereço
CREATE TABLE IF NOT EXISTS `person_address` (
  `person_id` BIGINT(11) UNSIGNED NOT NULL,
  `address_id` BIGINT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`person_id`, `address_id`),
  INDEX `idx_person_address_on_address_id` (`address_id` ASC),
  INDEX `idx_person_address_on_person_id` (`person_id` ASC),
  CONSTRAINT `fk_person_address_on_person_id`
    FOREIGN KEY (`person_id`)
    REFERENCES `person` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_person_address_on_address_id`
    FOREIGN KEY (`address_id`)
    REFERENCES `address` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
) ENGINE = InnoDB;

-- Criação da tabela de Categoria
create table if not exists category (
	`id` bigint(11) UNSIGNED not null auto_increment,
    `name` varchar(80) not null,
    `date_create` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,  
    `date_last_update` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    
    primary key(`id`)
) ENGINE=InnoDB;

-- Criação da tabela de SubCategoria
create table if not exists subcategory (
	`id` bigint(11) UNSIGNED not null auto_increment,
    `name` varchar(100) not null,
    `category_id` bigint(11) UNSIGNED NOT NULL,
    `date_create` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,  
    `date_last_update` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    
    primary key(`id`),
    CONSTRAINT `fk_subcategory_on_category_id`
    FOREIGN KEY (`category_id`)
    REFERENCES `category` (`id`)
) ENGINE=InnoDB;

-- Criação da tabela de Atuação
create table if not exists performance (
	`id` bigint(11) UNSIGNED not null auto_increment,
    `name` varchar(100) not null,
    `default_description` varchar(200) not null,
    `subcategory_id` bigint(11) UNSIGNED NOT NULL,
    `date_create` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,  
    `date_last_update` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    
    primary key(`id`),
    CONSTRAINT `fk_performance_on_subcategory_id`
    FOREIGN KEY (`subcategory_id`)
    REFERENCES `subcategory` (`id`)
) ENGINE=InnoDB;

-- Criação da tabela de Projeto
create table if not exists project (
	`id` bigint(11) UNSIGNED not null auto_increment,
    `name` varchar(100) not null,
    `star` INT(11) null DEFAULT 0,
    `value` DECIMAL(12,2) NULL DEFAULT 0.00,
    `performance_id` bigint(11) UNSIGNED NOT NULL,
    `date_create` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,  
    `date_last_update` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    
    primary key(`id`),
    CONSTRAINT `fk_project_on_performance_id`
    FOREIGN KEY (`performance_id`)
    REFERENCES `performance` (`id`)
) ENGINE=InnoDB;

-- Criação da tabela de relacionamento de Artista e Projeto
create table if not exists artist_project (
	`id` bigint(11) UNSIGNED not null auto_increment,
    `artist_id` bigint(11) UNSIGNED NOT NULL,
    `project_id` bigint(11) UNSIGNED NOT NULL,
    `date_create` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,  
    `date_last_update` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    
    primary key(`id`),
    CONSTRAINT `fk_artist_project_on_artist_id` FOREIGN KEY (`artist_id`) REFERENCES `person` (`id`),
    CONSTRAINT `fk_artist_project_on_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`)
) ENGINE=InnoDB;

-- Criação da tabela de Usuario
create table if not exists `user` (
	`id` bigint(11) UNSIGNED not null auto_increment,
    `password` varchar(100) not null,
    `token` varchar(100) not null,
    `token_refresh` varchar(100) not null,
    `person_id` bigint(11) UNSIGNED UNIQUE NOT NULL,
    `date_last_login` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `date_create` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,  
    `date_last_update` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    
    primary key(`id`),
    CONSTRAINT `fk_user_on_person_id`
    FOREIGN KEY (`person_id`)
    REFERENCES `person` (`id`)
) ENGINE=InnoDB;
