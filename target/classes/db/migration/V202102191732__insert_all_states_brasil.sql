insert into state (name, acronym) values('Acre', 'AC');  
insert into state (name, acronym) values('Alagoas', 'AL');  
insert into state (name, acronym) values('Amazonas', 'AM');
insert into state (name, acronym) values('Amapá', 'AP');
insert into state (name, acronym) values('Bahia', 'BA');
insert into state (name, acronym) values('Ceará', 'CE');
insert into state (name, acronym) values('Distrito Federal', 'DF');
insert into state (name, acronym) values('Espírito Santo', 'ES');
insert into state (name, acronym) values('Goiás', 'GO');
insert into state (name, acronym) values('Maranhão', 'MA');
insert into state (name, acronym) values('Minas Gerais', 'MG');
insert into state (name, acronym) values('Mato Grosso do Sul', 'MS');
insert into state (name, acronym) values('Mato Grosso', 'MT');
insert into state (name, acronym) values('Pará', 'PA');
insert into state (name, acronym) values('Paraíba', 'PB');
insert into state (name, acronym) values('Pernambuco', 'PE');
insert into state (name, acronym) values('Piauí', 'PI');
insert into state (name, acronym) values('Paraná', 'PR');
insert into state (name, acronym) values('Rio de Janeiro', 'RJ');
insert into state (name, acronym) values('Rio Grande do Norte', 'RN');
insert into state (name, acronym) values('Rondônia', 'RO');
insert into state (name, acronym) values('Roraima', 'RR');
insert into state (name, acronym) values('Rio Grande do Sul', 'RS');
insert into state (name, acronym) values('Santa Catarina', 'SC');
insert into state (name, acronym) values('Sergipe', 'SE');
insert into state (name, acronym) values('São Paulo', 'SP');
insert into state (name, acronym) values('Tocantins', 'TO');
