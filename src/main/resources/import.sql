insert into usuario(id, login, senha, data_cadastro, data_ultima_atualizacao) values (1, 'abilio1', '72600629F917', curdate(), curdate());
insert into usuario(id, login, senha, data_cadastro, data_ultima_atualizacao) values (2, 'abilio', '70CDA7BCDF191E', curdate(), curdate());
insert into usuario(id, login, senha, data_cadastro, data_ultima_atualizacao) values (3, 'jonhy', '678EBEF894', curdate(), curdate());

insert into cliente (id, nome, tipo, usuario_id, data_cadastro, data_ultima_atualizacao) values (1, 'Abilio', 2, 1, curdate(), curdate());
insert into cliente (id, nome, tipo, usuario_id, data_cadastro, data_ultima_atualizacao) values (2, 'Abilio Cliente', 1, 3, curdate(), curdate());
insert into cliente (id, nome, tipo, usuario_id, data_cadastro, data_ultima_atualizacao) values (3, 'Jhonny', 0, 2, curdate(), curdate());

insert into categoria (id, nome) values(1, 'Musicas');
insert into categoria (id, nome) values(2, 'Teatro');
insert into categoria (id, nome) values(3, 'Comedia');

insert into atuacao(id, descricao_padrao, nome, categoria_id) values (1, 'Animador de festas', 'DJ', 1);
insert into atuacao(id, descricao_padrao, nome, categoria_id) values (2, 'Animador de festas', 'Comediante', 3);

insert into cliente_atuacao(id, descricao, atuacao_id, cliente_id) values (1, '', 1, 1);
insert into cliente_atuacao(id, descricao, atuacao_id, cliente_id) values (2, 'Stand-up', 2, 3);

insert into estado (id, nome, data_cadastro, data_ultima_atualizacao) values (1, 'São Paulo', curdate(), curdate());
insert into estado (id, nome, data_cadastro, data_ultima_atualizacao) values (2, 'Minas Gerais', curdate(), curdate());

insert into cidade (id, nome, estado_id) values (1, 'Urbelândia', 2);
insert into cidade (id, nome, estado_id) values (2, 'Belo Horizonte', 2);
insert into cidade (id, nome, estado_id) values (3, 'Assis', 1);
insert into cidade (id, nome, estado_id) values (4, 'São Paulo', 1);

insert into projeto (id, cliente_atuacao_id, valor, estrelas) values (1, 1, 100.00, 5);
insert into projeto (id, cliente_atuacao_id, valor, estrelas) values (2, 2, 500.00, 8);
