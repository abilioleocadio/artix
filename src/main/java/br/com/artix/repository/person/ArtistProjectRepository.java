package br.com.artix.repository.person;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.artix.model.person.ArtistProject;

@Repository
public interface ArtistProjectRepository extends JpaRepository<ArtistProject, Long> {

}
