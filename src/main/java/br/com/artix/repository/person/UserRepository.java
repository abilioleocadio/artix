package br.com.artix.repository.person;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.artix.model.person.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	@Query("from User u "
		+ "inner join Person p on p.id = u.person.id " 
		+ "where p.phone = :token ")
	User findByPersonPhone(String token);

}
