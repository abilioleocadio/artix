package br.com.artix.repository.address;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.artix.model.address.City;

@Repository
public interface CityRepository extends JpaRepository<City, Long> {

	Optional<City> findById(Long id);

}
