package br.com.artix.repository.address;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.artix.model.address.State;

@Repository
public interface StateRepository extends JpaRepository<State, Long> {

	State findByName(String state);

}
