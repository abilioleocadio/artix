package br.com.artix.repository.performance;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.artix.model.performance.Performance;

@Repository
public interface PerformanceRepository extends JpaRepository<Performance, Long> {

	Performance findByName(String namePerformance);

}
