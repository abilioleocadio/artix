package br.com.artix.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.artix.model.person.User;

@Repository
public interface UsuarioRepository extends JpaRepository<User, Long> {

	User findByToken(String login);

}
