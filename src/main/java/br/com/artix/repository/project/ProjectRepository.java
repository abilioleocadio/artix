package br.com.artix.repository.project;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.artix.model.project.Project;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {

}
