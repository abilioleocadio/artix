package br.com.artix;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArtixApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArtixApplication.class, args);
	}

}
