package br.com.artix.service.category;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.artix.exception.EnumErrorException;
import br.com.artix.exception.ServiceException;
import br.com.artix.model.category.Category;
import br.com.artix.model.dto.CategoryDTO;
import br.com.artix.repository.category.CategoryRepository;

@Service
public class CategoryService {

	@Autowired
	private CategoryRepository repository;

	public List<Category> findAll() {
		return repository.findAll();
	}

	public Optional<Category> findById(Long categoryId) {
		return repository.findById(categoryId);
	}

	private Optional<Category> findByName(String name) {
		return repository.findByName(name);
	}

	@Transactional
	public Category create(CategoryDTO categoryDTO) throws ServiceException {

		Category category = new Category();
		if (findByName(categoryDTO.getName()).isPresent()) {
			throw new ServiceException(EnumErrorException.PARAMETROS_INVALIDOS, "Já existe essa categoria cadastrada!");
		}

		try {
			BeanUtils.copyProperties(categoryDTO, category, "id");
			return repository.save(category);
		} catch (Exception e) {
			throw new ServiceException(EnumErrorException.ERROR_SAVE, "Erro ao salvar categoria!");
		}
	}

	@Transactional
	public Category update(CategoryDTO categoryDTO, Long id) throws ServiceException {
		
		Optional<Category> category = findById(id);
		if (!category.isPresent()) {
			throw new ServiceException(EnumErrorException.PARAMETROS_INVALIDOS, "Essa categoria não está cadastrada!");
		}
		
		if (findByName(categoryDTO.getName()).isPresent()) {
			throw new ServiceException(EnumErrorException.PARAMETROS_INVALIDOS, 
					"Já existe outra categoria com o nome que deseja cadastrar!");
		}
		
		try {
			BeanUtils.copyProperties(categoryDTO, category.get(), "id");
			return repository.save(category.get());
		} catch (Exception e) {
			throw new ServiceException(EnumErrorException.ERROR_SAVE, "Erro ao salvar categoria!");
		}
	}

	public void deleteById(Long id) throws ServiceException {
		
		try {
			repository.deleteById(id);
		} catch (Exception e) {
			throw new ServiceException(EnumErrorException.ERROR_DELETE, "Erro ao excluir a categoria!");
		}
	}

}
