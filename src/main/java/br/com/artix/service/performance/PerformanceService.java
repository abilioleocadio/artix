package br.com.artix.service.performance;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.artix.exception.EnumErrorException;
import br.com.artix.exception.ServiceException;
import br.com.artix.model.dto.PerformanceDTO;
import br.com.artix.model.dto.PerformanceRegisterDTO;
import br.com.artix.model.performance.Performance;
import br.com.artix.model.person.ArtistProject;
import br.com.artix.model.person.Person;
import br.com.artix.model.project.Project;
import br.com.artix.model.subcategory.Subcategory;
import br.com.artix.repository.performance.PerformanceRepository;
import br.com.artix.service.person.ArtistProjectService;
import br.com.artix.service.person.PersonService;
import br.com.artix.service.project.ProjectService;
import br.com.artix.service.subcategory.SubcategoryService;
import br.com.artix.util.FunctionUtil;

@Service
public class PerformanceService {

	@Autowired private PerformanceRepository repository;
	@Autowired private PersonService personService;
	@Autowired private ProjectService projectService;
	@Autowired private ArtistProjectService artistProjectService;
	@Autowired private SubcategoryService subcategoryService;

	public List<Performance> findAll() {
		return repository.findAll();
	}
	
	public Optional<Performance> findById(Long id) {
		return repository.findById(id);
	}

	@Transactional
	public boolean register(PerformanceRegisterDTO performanceRegisterDTO) throws ServiceException {

		Optional<Person> person = personService.findById(performanceRegisterDTO.getArtistId());
		if(!person.isPresent()) {
			throw new ServiceException(EnumErrorException.PARAMETROS_INVALIDOS, "Não foi possível encontrar o artista informado!");
		}
		
		try {
			if(FunctionUtil.isEmpty(performanceRegisterDTO.getPerformanceId())) {
				person.get().setObservation(performanceRegisterDTO.getNamePerformance());
				personService.save(person.get());
			} else {
				Optional<Performance> performance = repository.findById(performanceRegisterDTO.getPerformanceId());
				if(performance.isPresent()) {
					Project project = new Project();
					project.setName(performance.get().getName());
					project.setPerformance(performance.get());
					projectService.save(project);
					
					ArtistProject artistProject = new ArtistProject();
					artistProject.setPerson(person.get());
					artistProject.setProject(project);
					artistProjectService.save(artistProject);
				} else {
					return false;
				}
			}
		} catch (Exception e) {
			throw new ServiceException(EnumErrorException.ERROR_SAVE);
		}

		return true;
	}

	@Transactional
	public Performance create(PerformanceDTO performanceDTO) throws ServiceException {

		Performance performance = new Performance();
		
		try {
			BeanUtils.copyProperties(performanceDTO, performance);
			
			Optional<Subcategory> subcategory = subcategoryService.findById(performanceDTO.getSubcategoryId());
			if(!subcategory.isPresent()) {
				throw new ServiceException(EnumErrorException.PARAMETROS_INVALIDOS, "Não foi possível encontrar a subcategoria!");
			}
			
			performance.setSubcategory(subcategory.get());
			return repository.save(performance);
		} catch (Exception e) {
			throw new ServiceException(EnumErrorException.ERROR_SAVE, "Erro ao salvar a atuação/habilidade!");
		}
		
	}

}
