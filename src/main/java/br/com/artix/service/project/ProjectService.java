package br.com.artix.service.project;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.artix.exception.EnumErrorException;
import br.com.artix.exception.ServiceException;
import br.com.artix.model.dto.ProjectDTO;
import br.com.artix.model.performance.Performance;
import br.com.artix.model.project.Project;
import br.com.artix.repository.project.ProjectRepository;
import br.com.artix.service.performance.PerformanceService;

@Service
public class ProjectService {

	@Autowired private ProjectRepository repository;
	@Autowired private PerformanceService performanceService;

	public List<Project> findAll() {
		return repository.findAll();
	}
	
	public Optional<Project> findById(Long id) {
		return repository.findById(id);
	}
	
	@Transactional
	public Project create(ProjectDTO projectDTO) throws ServiceException {
		
		Project project = new Project();
		Optional<Performance> performance = performanceService.findById(projectDTO.getPerformanceId());
		if(!performance.isPresent()) {
			throw new ServiceException(EnumErrorException.PARAMETROS_INVALIDOS, "Não foi possível encontrar a atuação/habilidade!");
		}
			
		try {
			BeanUtils.copyProperties(projectDTO, project);
			project.setPerformance(performance.get());
			return repository.save(project);
		} catch (Exception e) {
			throw new ServiceException(EnumErrorException.ERROR_SAVE, "Erro ao salvar project!");
		}
		
	}
	
	@Transactional
	public void save(Project project) {
		repository.save(project);
	}

}
