package br.com.artix.service.subcategory;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.artix.exception.EnumErrorException;
import br.com.artix.exception.ServiceException;
import br.com.artix.model.category.Category;
import br.com.artix.model.dto.SubcategoryDTO;
import br.com.artix.model.subcategory.Subcategory;
import br.com.artix.repository.subcategory.SubcategoryRepository;
import br.com.artix.service.category.CategoryService;
import br.com.artix.util.FunctionUtil;

@Service
public class SubcategoryService {

	@Autowired private SubcategoryRepository repository;
	@Autowired private CategoryService categoryService;

	public List<Subcategory> findAll() {
		return repository.findAll();
	}
	
	public Optional<Subcategory> findById(Long subCategoryId) {
		return repository.findById(subCategoryId);
	}
	
	private Optional<Subcategory> findByName(String name) {
		return repository.findByName(name);
	}

	@Transactional
	public Subcategory create(SubcategoryDTO subcategoryDTO) throws ServiceException {

		Subcategory subcategory = new Subcategory();
		if(findByName(subcategoryDTO.getName()).isPresent()) {
			throw new ServiceException(EnumErrorException.PARAMETROS_INVALIDOS, "Já existe está subcategoria cadastrada!");
		}
		
		Optional<Category> category = categoryService.findById(subcategoryDTO.getCategoryId());
		if(!category.isPresent()) {
			throw new ServiceException(EnumErrorException.PARAMETROS_INVALIDOS, "Não foi possível encontrar a categoria!");
		}
			
		try {
			BeanUtils.copyProperties(subcategoryDTO, subcategory);
			subcategory.setCategory(category.get());
			return repository.save(subcategory);
		} catch (Exception e) {
			throw new ServiceException(EnumErrorException.ERROR_SAVE, "Erro ao salvar subcategoria!");
		}
	}

	@Transactional
	public Subcategory update(@Valid SubcategoryDTO subcategoryDTO, Long id) throws ServiceException {
		
		Optional<Subcategory> subcategory = findById(id);
		if(!subcategory.isPresent()) {
			throw new ServiceException(EnumErrorException.PARAMETROS_INVALIDOS, "Não foi possível encontrar a subcategoria!");
		}
		
		Category category = categoryService.findById(subcategoryDTO.getCategoryId()).orElse(null);
		if(FunctionUtil.isEmpty(category)) {
			throw new ServiceException(EnumErrorException.PARAMETROS_INVALIDOS, "Não foi possível encontrar a categoria!");
		}
		
		if(findByName(subcategoryDTO.getName()).isPresent()) {
			throw new ServiceException(EnumErrorException.PARAMETROS_INVALIDOS, "Já existe está subcategoria cadastrada!");
		}
		
		try {
			BeanUtils.copyProperties(subcategoryDTO, subcategory.get(), "id");
			subcategory.get().setCategory(category);
			return repository.save(subcategory.get());
		} catch (Exception e) {
			throw new ServiceException(EnumErrorException.ERROR_SAVE, "Erro ao salvar a subcategoria!");
		}
		
	}

	public void deleteById(Long id) throws ServiceException {
		try {
			repository.deleteById(id);
		} catch (Exception e) {
			throw new ServiceException(EnumErrorException.ERROR_DELETE, "Não foi possível excluir a subcategoria!");
		}
	}

}
