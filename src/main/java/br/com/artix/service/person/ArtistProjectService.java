package br.com.artix.service.person;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.artix.model.person.ArtistProject;
import br.com.artix.repository.person.ArtistProjectRepository;

@Service
public class ArtistProjectService {

	@Autowired private ArtistProjectRepository repository;

	@Transactional
	public void save(ArtistProject artistProject) {
		repository.save(artistProject);
	}
	
}
