package br.com.artix.service.person;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.artix.exception.EnumErrorException;
import br.com.artix.exception.ServiceException;
import br.com.artix.model.dto.PersonRegisterDTO;
import br.com.artix.model.person.Person;
import br.com.artix.repository.person.PersonRepository;
import br.com.artix.util.FunctionUtil;

@Service
public class PersonService {

	@Autowired private PersonRepository repository;
	@Autowired private UserService userService;
	
	public List<Person> findAll() {
		return repository.findAll();
	}

	public Optional<Person> findById(Long id) {
		return repository.findById(id);
	}

	@Transactional
	public Person register(PersonRegisterDTO personDTO) throws ServiceException {
		
			Person person = new Person();
			if(validatePhone(personDTO.getPhone())) {
				throw new ServiceException(EnumErrorException.PARAMETROS_INVALIDOS, "Este número de telefone já existe!");
			}
			
		try {
			BeanUtils.copyProperties(personDTO, person, "id");
		
			save(person);
			userService.register(personDTO.getPassword(), person);
			return person;
		} catch (Exception e) {
			throw new ServiceException(EnumErrorException.ERROR_SAVE_PERSON, "Erro ao salvar o artista no banco!");
		}
		
	}

	private boolean validatePhone(String phone) {
		
		Optional<Person> person = repository.findByPhone(FunctionUtil.removerMascaraTexto(phone));
		
		return person.isPresent();
	}

	@Transactional
	public void save(Person person) {
		repository.save(person);
	}

}
