package br.com.artix.service.person;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.artix.exception.EnumErrorException;
import br.com.artix.exception.ServiceException;
import br.com.artix.model.person.Person;
import br.com.artix.model.person.User;
import br.com.artix.repository.person.UserRepository;
import br.com.artix.util.Cript;
import br.com.artix.util.FunctionUtil;

@Service
public class UserService {

	@Autowired private UserRepository repository;

	public void register(String password, Person person) throws ServiceException {

		try {
			User user = new User();
			user.setPerson(person);
			user.setPassword(Cript.encripta(password));
			user.setToken(Cript.encripta(
					(FunctionUtil.removerMascaraTexto(person.getPhone()) 
						+ password)
					));
			user.setTokenRefresh(Cript.encripta(
					(FunctionUtil.removerMascaraTexto(person.getPhone()) 
							+ password)
						));
			
			repository.save(user);
		} catch (ServiceException e) {
			throw new ServiceException(EnumErrorException.ERROR_SAVE_PERSON, "Erro ao salvar o usuário no banco!");
		}
		
	}

	public User findUserByToken(String token) {
		return repository.findByPersonPhone(token);
	}
	
}
