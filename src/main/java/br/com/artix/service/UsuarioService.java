package br.com.artix.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.artix.model.person.User;
import br.com.artix.repository.UsuarioRepository;

@Service
public class UsuarioService {
	
	@Autowired private UsuarioRepository usuarioRepository;

	public User findByLogin(String login) {
		return usuarioRepository.findByToken(login);
	}

}
 