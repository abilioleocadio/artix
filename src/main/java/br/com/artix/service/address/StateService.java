package br.com.artix.service.address;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import br.com.artix.exception.EntidadeEmUsoException;
import br.com.artix.exception.EntidadeNaoEncontradaException;
import br.com.artix.exception.ServiceException;
import br.com.artix.model.address.State;
import br.com.artix.model.dto.StateDTO;
import br.com.artix.repository.address.StateRepository;

@Service
public class StateService {

	@Autowired private StateRepository repository;
	
	public List<State> findAll() {
		return repository.findAll();
	}

	public ResponseEntity<State> findById(Long id) {

		Optional<State> state = repository.findById(id);
		if (!state.isPresent()) {
			return ResponseEntity.notFound().build();
		}

		return ResponseEntity.ok(state.get());
	}

	public State create(StateDTO stateDTO) throws ServiceException {
		State state = new State();
		
		BeanUtils.copyProperties(stateDTO, state);
		return repository.save(state);
	}
	
	public ResponseEntity<State> update(Long id, StateDTO stateDTO) throws ServiceException {

		Optional<State> state = repository.findById(id);
		if (state.isPresent()) {
			BeanUtils.copyProperties(stateDTO, state.get(), "id");
			return ResponseEntity.ok(repository.save(state.get()));
		}

		return ResponseEntity.notFound().build();
	}

	public ResponseEntity<?> delete(Long id) throws ServiceException {

		try {
			repository.deleteById(id);
			return ResponseEntity.noContent().build();
			
		} catch (EntidadeNaoEncontradaException e) {
			return ResponseEntity.notFound().build();
			
		} catch (EntidadeEmUsoException e) {
			return ResponseEntity.status(HttpStatus.CONFLICT).body(e.getMessage());
		}
		
	}

	public State findByName(String state) {
		return repository.findByName(state);
	}

}
