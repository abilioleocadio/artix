package br.com.artix.service.address;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import br.com.artix.exception.EntidadeEmUsoException;
import br.com.artix.exception.EntidadeNaoEncontradaException;
import br.com.artix.exception.ServiceException;
import br.com.artix.model.address.City;
import br.com.artix.model.address.State;
import br.com.artix.model.dto.CityDTO;
import br.com.artix.repository.address.CityRepository;
import br.com.artix.util.FunctionUtil;

@Service
public class CityService {

	@Autowired private CityRepository repository;
	@Autowired private StateService stateService;
	
	public List<City> findAll() {
		return repository.findAll();
	}

	public City findById(Long id) {
		return repository.findById(id).orElse(null);
	}

	public City create(CityDTO cityDTO) throws ServiceException {
		City city = new City();
		
		BeanUtils.copyProperties(cityDTO, city);
		return repository.save(city);
	}
	
	public ResponseEntity<City> update(Long id, CityDTO cityDTO) {

		Optional<City> city = repository.findById(id);
		if (city.isPresent()) {
			BeanUtils.copyProperties(cityDTO, city.get(), "id");
			
			State state = stateService.findByName(cityDTO.getState());
			if(FunctionUtil.isEmpty(state)) {
				return ResponseEntity.notFound().build();
			}
			
			city.get().setState(state);
			return ResponseEntity.ok(repository.save(city.get()));
		}

		return ResponseEntity.notFound().build();
		
	}

	public ResponseEntity<?> delete(Long id) {

		try {
			repository.deleteById(id);
			return ResponseEntity.noContent().build();
			
		} catch (EntidadeNaoEncontradaException e) {
			return ResponseEntity.notFound().build();
			
		} catch (EntidadeEmUsoException e) {
			return ResponseEntity.status(HttpStatus.CONFLICT).body(e.getMessage());
		}
		
	}
	
}
