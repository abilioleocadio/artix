package br.com.artix.controller.performance;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.artix.model.dto.PerformanceDTO;
import br.com.artix.model.dto.PerformanceRegisterDTO;
import br.com.artix.service.performance.PerformanceService;

@RestController
@RequestMapping("/performances")
public class PerformanceController {

	@Autowired private PerformanceService service;
	
	@GetMapping
	public ResponseEntity<?> findAll() {
		return ResponseEntity.ok(service.findAll());
	}
	
	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping
	public ResponseEntity<?> create(@Valid @RequestBody PerformanceDTO performanceDTO) {
		return ResponseEntity.ok(service.create(performanceDTO));
	}
	
	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping("/register")
	public ResponseEntity<?> register(@Valid @RequestBody PerformanceRegisterDTO performanceRegisterDTO) {
		if(service.register(performanceRegisterDTO)) {
			return ResponseEntity.ok().build();
		} else {
			return ResponseEntity.badRequest().build();
		}
	}
	
}
