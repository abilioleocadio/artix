package br.com.artix.controller.subcategory;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.artix.model.dto.SubcategoryDTO;
import br.com.artix.model.subcategory.Subcategory;
import br.com.artix.service.subcategory.SubcategoryService;

@RestController
@RequestMapping("subcategories")
public class SubcategoryController {

	@Autowired private SubcategoryService service;
	
	@GetMapping
	public ResponseEntity<?> findAll() {
		return ResponseEntity.ok(service.findAll());
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> findById(@PathVariable(name = "id") Long id) {
		Optional<Subcategory> subcategory = service.findById(id);
		if(subcategory.isPresent()) {
			return ResponseEntity.ok(subcategory.get());
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Subcategory create(@Valid @RequestBody SubcategoryDTO subcategoryDTO) {
		return service.create(subcategoryDTO);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<?> update(@Valid @RequestBody SubcategoryDTO subcategoryDTO,
			@PathVariable(name = "id") Long id) {
		return ResponseEntity.ok(service.update(subcategoryDTO, id));
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> delete(@PathVariable(name = "id") Long id) {
		Optional<Subcategory> subcategory = service.findById(id);
		if(!subcategory.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		service.deleteById(id);
		return ResponseEntity.noContent().build();
	}
	
}
