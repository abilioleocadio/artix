package br.com.artix.controller.person;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.artix.model.dto.PersonRegisterDTO;
import br.com.artix.model.person.Person;
import br.com.artix.service.person.PersonService;

@RestController
@RequestMapping("/people")
public class PersonController {

	@Autowired private PersonService service;
	
	@GetMapping
	public ResponseEntity<?> findAll() {
		return ResponseEntity.ok(service.findAll());
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> findById(@PathVariable("id") Long id) {
		Optional<Person> person = service.findById(id);
		if(person.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok(person.get());
	}
	
	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping("/register")
	public ResponseEntity<?> register(@Valid @RequestBody PersonRegisterDTO personDTO) {
		return ResponseEntity.ok(service.register(personDTO));
	}
	
}
