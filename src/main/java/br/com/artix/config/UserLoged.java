package br.com.artix.config;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import br.com.artix.model.enums.TypePerson;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserLoged implements UserDetails {

	private static final long serialVersionUID = 1L;
	private Integer id;
	private String nomeUsuario;
	private String username;
	private String password;
	private TypePerson tipo;
	private Set<GrantedAuthority> authorities;
	private boolean accountNonExpired;
	private boolean accountNonLocked;
	private boolean credentialsNonExpired;
	private boolean enabled;
	private boolean clientOnly;

	public UserLoged() {
		this.authorities = new HashSet<GrantedAuthority>();
	}

	public UserLoged(Integer id, String nomeUsuario, String username, String password, TypePerson tipo, boolean enabled,
			boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked,
			Collection<? extends GrantedAuthority> authorities) {
		this();
		this.id = id;


		this.nomeUsuario = nomeUsuario;
		this.username = username;
		this.password = password;
		this.tipo = tipo;
		this.enabled = enabled;
		this.accountNonExpired = accountNonExpired;
		this.credentialsNonExpired = credentialsNonExpired;
		this.accountNonLocked = accountNonLocked;
		this.authorities.addAll(authorities);

	}

}
