package br.com.artix.config;

import org.springframework.security.crypto.password.PasswordEncoder;

import br.com.artix.util.Cript;

public class VSMPasswordEncoder implements PasswordEncoder {

	@Override
	public String encode(CharSequence pass) {
		String passStr = String.valueOf(pass);
		return Cript.encripta(passStr);
	}

	@Override
	public boolean matches(CharSequence rawPass, String encripted) {
		String x = Cript.encripta(String.valueOf(rawPass));
		return x.equals(encripted);
	}
	
}
