package br.com.artix.config;

import java.io.Serializable;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Abílio Leocádio Arruda de Souza
 */
@Component
@Getter
@Setter
public class UsuarioLogin implements Serializable {

	private static final long serialVersionUID = 796418067408342968L;
    private Integer id;
    private String apelido;
    private String email;
    private String cpf;
    private Integer idPerfil;
    private Set<GrantedAuthority> permissoes;
    private Integer minutosExpirarToken;
    
    /**
     * invalida o vendedor logado (logout)
     */
    public void invalidar() {
        this.id = null;
        this.apelido = null;
        this.email = null;
        this.cpf = null;
        this.idPerfil = null;
        this.permissoes = null;
        this.minutosExpirarToken = null;
    }

    /**
     * Cria uma "instancia" baseado no usuario passado como parametro
     *
     * @param vendedor
     */
    public void set(UserLoged usuario) {
        if (usuario == null) {
            return;
        }

        id = usuario.getId();
//        apelido = usuario.getLogin();
        email = "";
        cpf = "";
        idPerfil = 0;
    }
    
}
