package br.com.artix.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;

@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

	 @Autowired
	    @Qualifier("UserDetailServiceVSM")
	    private UserDetailsService userDetailsService;

	    @Autowired
	    public void configure(AuthenticationManagerBuilder builder) throws Exception {
	        builder.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
	    }

	    @Override
	    public void configure(HttpSecurity http) throws Exception {
	        http
	                .authorizeRequests()

	                //Requisições usadas para contratação on-line.
	                .antMatchers("/oauth/**").permitAll()
	                .antMatchers("/user/**").permitAll()
	                .antMatchers("/public/**").permitAll()

	                .anyRequest().authenticated()
	                .and()

	                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
	                .and()

	                .csrf().disable();
	    }

	    @Override
	    public void configure(ResourceServerSecurityConfigurer resources) {
	        resources.stateless(false);
	    }

	    @Bean
	    public PasswordEncoder passwordEncoder() {
	        return new VSMPasswordEncoder();
	    }


	    // classe auxiliar
	    @Configuration
	    @EnableWebSecurity
	    public static class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	        @Bean
	        @Override
	        public AuthenticationManager authenticationManagerBean() throws Exception {
	            return super.authenticationManagerBean();
	        }

	    }
	
}
