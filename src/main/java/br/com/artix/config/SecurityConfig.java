//package br.com.artix.config;
//
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//
//@Configuration
//public class SecurityConfig extends WebSecurityConfigurerAdapter {
//	
//	@Override
//  public void configure(HttpSecurity clients) throws Exception {
//      clients.inMemory()
//              .withClient("angular") // Client do postman
//              .secret("726CD0C6063953") // Client do postman mas encriptografado
//              .scopes("openid")
//              .authorizedGrantTypes("client_credentials", "password", "refresh_token")
//              .accessTokenValiditySeconds(1800)
//              .refreshTokenValiditySeconds(24 * 3600)
//      ;
//  }
//
//}
