package br.com.artix.config;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.com.artix.model.enums.TypePerson;
import br.com.artix.model.person.User;
import br.com.artix.service.person.UserService;

/**
 * @author Abílio Leocádio Arruda de Souza 
 */
@Service(value = "UserDetailServiceVSM")
public class UserDetailServiceVSM implements UserDetailsService {

    @Autowired private UserService userService;
//    @Autowired private LogService logService;
//    @Autowired private PermissionProperties permissionProperties;
    @Autowired private UsuarioLogin usuarioLogin;
    
    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
    	
        if (login == null) {
            throw new UsernameNotFoundException("Usuário não informado corretamente!");
        }
        
        User user = new User(); 
        try {
        	user = userService.findUserByToken(login);
        } catch (ServiceException e) {
        	throw new UsernameNotFoundException(e.getMessage());
        }
        
        if(user == null) {
        	throw new UsernameNotFoundException("Usuário não informado corretamente");
        }
        
        return retornarColaborador(user);

    }
    
    private UserDetails retornarColaborador(User user) {
    	boolean permissao = true;
    	
//        if(vendedor.getStatus().equals(EntitySituacao.A)) {
//        	permissao = true;
//        }
    	
//    	 User user = new User(vendedor.getId(), vendedor.getApelido(), vendedor.getEmail(), 
//    			vendedor.getSenha(), permissao, true, true, true, getAuthorities(vendedor));
    	
    	UserLoged userLoged = new UserLoged(user.getId().intValue(), user.getToken(), user.getToken(), user.getPassword(), 
    			TypePerson.ADMINISTRADOR, permissao, true, true, true, getAuthorities(user));
    	 usuarioLogin.set(userLoged);
    	 usuarioLogin.setPermissoes(userLoged.getAuthorities());
    	 return userLoged;
    }

    private Collection<? extends GrantedAuthority> getAuthorities(User usuario) {
    	
    	Set<SimpleGrantedAuthority> authorities = new HashSet<>();
    	List<String> autority = new ArrayList<String>();

		try {
//			if(usuario.getTipo().equals(TipoUsuarioEnum.ADMINISTRADOR)) {
//				autority.add("cadastro_artista");
//			}
//			for (Integer permission : permissionProperties.getADM()) {
//				if(permission.equals(colaborador.getIdPerfil())) {
//					autority.add("ativacao_contrato_crud");
//					autority.add("ativacao_contrato_ativar");
//				}
//			}
//			for (Integer permission : permissionProperties.getCOMERCIAL()) {
//				if(permission.equals(colaborador.getIdPerfil())) {
//					autority.add("cadastro_conexao_crud");
//					autority.add("ativacao_contrato_crud");
//					autority.add("ativacao_contrato_ativar");
//					autority.add("notificacao_crud");
//				}
//			}
//			for (Integer permission : permissionProperties.getDESENVOLVIMENTO()) {
//				if(permission.equals(colaborador.getIdPerfil())) {
//					autority.add("ativacao_contrato_crud");
//					autority.add("ativacao_contrato_ativar");
//					autority.add("cadastro_conexao_crud");
//					autority.add("acesso_desenvolvimento");
//					autority.add("notificacao_crud");
//				}
//			}
//			for (Integer permission : permissionProperties.getSUPORTE()) {
//				if(permission.equals(colaborador.getIdPerfil())) {
//					autority.add("cadastro_conexao_crud");
//					autority.add("notificacao_crud");
//				}
//			}
			autority.forEach(value -> {
				authorities.add(new SimpleGrantedAuthority(value));
			});
		} catch (Exception e) {
//			logService.logErro(ObjetoLog.SISTEMA, e.getMessage());
			throw e;
		}


        return authorities;
    }

}
