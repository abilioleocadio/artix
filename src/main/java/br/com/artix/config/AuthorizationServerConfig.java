package br.com.artix.config;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.security.oauth2.provider.error.DefaultWebResponseExceptionTranslator;
import org.springframework.security.oauth2.provider.error.WebResponseExceptionTranslator;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestFilter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Abílio Leocádio Arruda de Souza
 */
@Configuration
@EnableAuthorizationServer
@EnableGlobalMethodSecurity(securedEnabled = true)
@RestController
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

	@Lazy
	@Autowired private AuthenticationManager authenticationManager;

    @Autowired private UsuarioLogin usuarioLogin;
    
	private static final String REFRESHTOKENERROR = "Invalid refresh token";
	private static final String USUARIOSENHAINCORRETO = "Bad credentials";
	
	@Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients.inMemory()
                .withClient("angular") // Client do postman
                .secret("726CD0C6063953") // Client do postman mas encriptografado
                .scopes("openid")
                .authorizedGrantTypes("client_credentials", "password", "refresh_token")
                .accessTokenValiditySeconds(1800)
                .refreshTokenValiditySeconds(24 * 3600)
        ;
    }
	
	@Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        final TokenEnhancerChain tokenEnhancerChain = new TokenEnhancerChain();
        tokenEnhancerChain.setTokenEnhancers(Arrays.asList(tokenEnhancer()));

        endpoints
                .tokenStore(tokenStore())
                .reuseRefreshTokens(false)
                .authenticationManager(authenticationManager)
                .tokenEnhancer(tokenEnhancerChain)
                .exceptionTranslator(webResponseExceptionTranslator())
        ;
    }
	@Bean
    public TokenStore tokenStore() {
        return new InMemoryTokenStore();
    }

    @Bean
    public TokenEnhancer tokenEnhancer() {
        return new VSMTokenEnhancer();
    }


    @GetMapping(path = "/oauth/user")
    public ResponseEntity<?> user(OAuth2Authentication user) throws Exception {
        if (user == null || !user.isAuthenticated()) {
            return ResponseEntity
                    .status(HttpStatus.UNAUTHORIZED)
                    .body(new String("Usuário não autorizado".getBytes("UTF-8")));
        }

        OAuth2AuthenticationDetails details = (OAuth2AuthenticationDetails) user.getDetails();
        OAuth2AccessToken token = tokenStore().readAccessToken(details.getTokenValue());

        usuarioLogin.setMinutosExpirarToken(token.getExpiresIn());

        return ResponseEntity.ok(usuarioLogin);
    }

    @GetMapping(path = "/user")
    public ResponseEntity<?> getAccessToken(OAuth2Authentication user) throws Exception {
        if (user == null || !user.isAuthenticated()) {
            return ResponseEntity
                    .status(HttpStatus.UNAUTHORIZED)
                    .body(new String("Usuário não autorizado".getBytes("UTF-8")));
        }

        OAuth2AuthenticationDetails details = (OAuth2AuthenticationDetails) user.getDetails();
        OAuth2AccessToken token = tokenStore().readAccessToken(details.getTokenValue());

        return ResponseEntity.ok(token);
    }

    @GetMapping(path = "/oauth/logout")
    public ResponseEntity<?> logout(OAuth2Authentication user) throws Exception {
        if (user == null) {
            return ResponseEntity.ok().build();
        }

        try {
            OAuth2AccessToken token = tokenStore().getAccessToken(user);
            tokenStore().removeAccessToken(token);
            tokenStore().removeRefreshToken(token.getRefreshToken());
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            return ResponseEntity
                    .status(HttpStatus.UNAUTHORIZED)
                    .body(new String("Falha ao realizar logout".getBytes("UTF-8")));
        }
    }

    @Bean
	public WebResponseExceptionTranslator<OAuth2Exception> webResponseExceptionTranslator() {
		return new DefaultWebResponseExceptionTranslator() {

			@Override
			public ResponseEntity<OAuth2Exception> translate(Exception e) throws Exception {
				ResponseEntity<OAuth2Exception> responseEntity = super.translate(e);

				if (responseEntity.getBody().getMessage().equals(USUARIOSENHAINCORRETO)) {
					return new ResponseEntity<>(
							new OAuth2Exception(new String("Usuario e/ou senha inválidos".getBytes("UTF-8"))),
							responseEntity.getStatusCode());
				} else {
					if (responseEntity.getBody().getMessage().contains((REFRESHTOKENERROR))) {

						return new ResponseEntity<>(
								new OAuth2Exception(new String("semRefreshToken".getBytes("UTF-8"))),
								responseEntity.getStatusCode());

					} else {
						return new ResponseEntity<>(
								new OAuth2Exception(
										new String(responseEntity.getBody().getMessage().getBytes("UTF-8"))),
								responseEntity.getStatusCode());

					}

				}

			}
		};
	}

    @Bean
    public SecurityContextHolderAwareRequestFilter securityContextHolderAwareRequestFilter() {
        return new SecurityContextHolderAwareRequestFilter();
    }
	
}
