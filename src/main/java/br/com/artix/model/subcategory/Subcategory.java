package br.com.artix.model.subcategory;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import br.com.artix.model.category.Category;
import br.com.artix.model.util.AbstractEntity;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "subcategory")
public class Subcategory extends AbstractEntity {

	@NotBlank(message = "nome é obrigatório!")
	private String name;
	
	@ManyToOne
	@JoinColumn(name="category_id", nullable = false)
	private Category Category;
	
}
