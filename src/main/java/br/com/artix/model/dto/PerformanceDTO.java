package br.com.artix.model.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PerformanceDTO {

	private Long id;
	
	@NotBlank(message = "Nome é obrigatório!")
	private String name;
	
	private String defaultDescription;
	
	@NotNull(message = "Subcategoria é obrigatória!")
	private Long subcategoryId;
	
}
