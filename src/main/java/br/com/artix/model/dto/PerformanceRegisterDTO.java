package br.com.artix.model.dto;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PerformanceRegisterDTO {

	@NotNull(message = "Artista é obrigatório!")
	private Long artistId;
	
	private Long performanceId;
	
	private String namePerformance;
	
}
