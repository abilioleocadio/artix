package br.com.artix.model.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProjectDTO {

	@NotBlank(message = "nome é obrigatório!")
	private String name;
	private Integer star;
	private Double value;
	
	@NotNull(message = "atuação/habilidade é obrigatória!")
	private Long performanceId;
	
}
