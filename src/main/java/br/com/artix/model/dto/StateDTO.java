package br.com.artix.model.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
public class StateDTO {

	@NotBlank(message = "nome é obrigatório!")
	public String name;
	
	@Size(max = 2, message = "máximo 2 caracteres permitidos!")
	@NotBlank(message = "sigla é obrigatório!")
	public String acronym;
	
}
