package br.com.artix.model.dto;

import javax.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CategoryDTO {

	@NotBlank(message = "Nome é obrigatório!")
	private String name;
	
}
