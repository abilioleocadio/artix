package br.com.artix.model.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import br.com.artix.model.enums.TypePerson;
import lombok.Data;

@Data
public class PersonRegisterDTO {

	@NotNull(message = "Tipo é obrigatório!")
	private TypePerson type;
	
	@NotBlank(message = "E-mail é obrigatório!")
	private String email;
	
	@NotBlank(message = "Telefone é obrigatório!")
	private String phone;
	
	@NotBlank(message = "Nome é obrigatório!")
	private String name;
	
	@NotBlank(message = "Senha é obrigatório!")
	private String password;
	
}
