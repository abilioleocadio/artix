package br.com.artix.model.dto;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class CityDTO {

	@NotBlank(message = "nome é obrigatório!")
	private String name;
	
	@NotBlank(message = "estado é obrigatório!")
	private String state;
	
}
