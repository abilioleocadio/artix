package br.com.artix.model.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SubcategoryDTO {

	@NotBlank(message = "Nome é obrigatório!")
	private String name;
	
	@NotNull(message = "Categoria é obrigatório!")
	private Long categoryId;
	
}
