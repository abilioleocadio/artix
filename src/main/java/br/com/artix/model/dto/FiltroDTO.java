package br.com.artix.model.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FiltroDTO<T> implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private T obj;
    private int page = 0;
    private int size = 7;
	
}
