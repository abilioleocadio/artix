package br.com.artix.model.project;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import br.com.artix.model.performance.Performance;
import br.com.artix.model.util.AbstractEntity;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "project")
public class Project extends AbstractEntity {

	@NotBlank(message = "nome é obrigatório!")
	private String name;
	
	private Integer star;
	
	private BigDecimal value;
	
	@ManyToOne
	@JoinColumn(name = "performance_id", nullable = false)
	private Performance performance;
	
}
