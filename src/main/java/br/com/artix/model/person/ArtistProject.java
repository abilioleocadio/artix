package br.com.artix.model.person;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.artix.model.project.Project;
import br.com.artix.model.util.AbstractEntity;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "artist_project")
public class ArtistProject extends AbstractEntity {

	@ManyToOne
	@JoinColumn(name = "artist_id", nullable = false)
	private Person person;
	
	@ManyToOne
	@JoinColumn(name = "project_id", nullable = false)
	private Project project;
	
}
