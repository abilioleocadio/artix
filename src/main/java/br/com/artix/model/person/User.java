package br.com.artix.model.person;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import br.com.artix.model.util.AbstractEntity;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "user")
public class User extends AbstractEntity {

	@NotBlank(message = "password é obrigatório")
	private String password;
	
	@NotBlank(message = "token é obrigatório")
	private String token;
	
	@NotBlank(message = "refresh token é obrigatório")
	@Column(name = "token_refresh")
	private String tokenRefresh;
	
	@NotNull(message = "person é obrigatório")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "person_id")
	private Person person;
	
}
