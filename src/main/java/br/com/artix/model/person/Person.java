package br.com.artix.model.person;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.com.artix.model.address.Address;
import br.com.artix.model.enums.TypePerson;
import br.com.artix.model.util.AbstractEntity;
import br.com.artix.util.FunctionUtil;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "person")
public class Person extends AbstractEntity {
	
	@NotNull(message = "tipo é obrigatório!")
	@Enumerated(EnumType.ORDINAL)
	private TypePerson type;
	
	@NotBlank(message = "nome é obrigatório!")
	private String name;
	
	@Size(max = 15, message = "máximo 15 caracteres!")
	private String cpf;
	
	@NotBlank(message = "email é obrigatório!")
	private String email;
	
	@Size(max = 16, message = "máximo 16 caracteres!")
	@NotBlank(message = "telefone é obrigatório!")
	private String phone;
	
	private String observation;
	
	@OneToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "person_address", joinColumns = { @JoinColumn(name = "person_id") }, inverseJoinColumns = {
			@JoinColumn(name = "address_id") })
	@OrderBy("address")
	private Set<Address> addresses;
	
	public void setPhone(String phone) {
		if(!FunctionUtil.isEmpty(phone)) {
			this.phone = FunctionUtil.removerMascaraTexto(phone);
		}
	}

}
