package br.com.artix.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.sun.istack.NotNull;

import lombok.Data;
import lombok.EqualsAndHashCode;

//@Data
//@EqualsAndHashCode(onlyExplicitlyIncluded = true)
//@Entity
//@Table(name = "projeto")
public class Projeto {

//	@EqualsAndHashCode.Include
//	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
//	private Long id;
	
//	@ManyToOne
//    @JoinColumn(name="cliente_atuacao_id", nullable = false)
//	private ClienteAtuacao clienteAtuacao;
	
	@NotNull
	@Column(nullable = true)
	private Double valor;
	
	private Integer estrelas;
}
