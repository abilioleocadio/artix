package br.com.artix.model.enums;

import lombok.Getter;

@Getter
public enum TipoUsuarioEnum {

	CLIENTE(0, "cliente"),
	ARTISTA(1, "artista"),
	ADMINISTRADOR(2, "administrador");
	
	private Integer codigo;
	private String valor;
	
	private TipoUsuarioEnum(Integer codigo, String valor) {
		this.codigo = codigo;
		this.valor = valor;
	}
	
}
