package br.com.artix.model.enums;

import lombok.Getter;

@Getter
public enum TypePerson {
	
	ARTISTA(0, "administrador"),
	CLIENTE(1, "artista"),
	ADMINISTRADOR(2, "cliente");
	
	private Integer codigo;
    private String valor;
    
    private TypePerson(Integer codigo, String valor) {
        this.codigo = codigo;
    	this.valor = valor;
    }

}
