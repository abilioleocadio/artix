package br.com.artix.model.util;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@MappedSuperclass
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public abstract class AbstractEntity {

//	@EqualsAndHashCode.Include
//	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
//	private Long id;
	
	@Id
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
	private Long id;
	
	@JsonIgnore
	@Column(name = "date_create", nullable = false, updatable = false)
    @CreationTimestamp
    private LocalDateTime dateCreate;
	
	@JsonIgnore
	@Column(name = "date_last_update")
	@UpdateTimestamp
    private LocalDateTime dateLastUpdate;
	
//	@PrePersist
//    protected void prePersist() {
//        if (this.dataCadastro == null) dataCadastro = LocalDate.now();
//        if (this.dataUltimaAtualizacao == null) dataUltimaAtualizacao = LocalDate.now();
//    }
//
//    @PreUpdate
//    protected void preUpdate() {
//        this.dataUltimaAtualizacao = LocalDate.now();
//    }
	
}
