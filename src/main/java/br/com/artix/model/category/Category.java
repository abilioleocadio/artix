package br.com.artix.model.category;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import br.com.artix.model.util.AbstractEntity;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "category")
public class Category extends AbstractEntity {

	@NotBlank(message = "nome é obrigatório!")
	private String name;
	
}
