package br.com.artix.model.address;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import br.com.artix.model.util.AbstractEntity;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "state")
public class State extends AbstractEntity {

	@NotBlank(message = "nome é obrigatório!")
	private String name;
	
	@Size(max = 2, message = "máximo 2 caracteres permitidos!")
	@NotBlank(message = "sigla é obrigatório!")
	private String acronym;
	
}
