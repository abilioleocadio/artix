package br.com.artix.model.address;

import javax.persistence.Entity;
import javax.persistence.Table;

import br.com.artix.model.util.AbstractEntity;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "address")
public class Address extends AbstractEntity {

}
