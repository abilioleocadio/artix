package br.com.artix.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

//@Data
//@EqualsAndHashCode(onlyExplicitlyIncluded = true)
//@Entity
//@Table(name = "cliente_atuacao")
public class ClienteAtuacao {
	
//	@EqualsAndHashCode.Include
//	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
//	private Long id;
	
	@Column(name = "descricao")
	private String descricao;
	
//	@ManyToOne
//	@JoinColumn(name="atuacao_id", nullable = false)
//	private Atuacao atuacao;
//	
//	@ManyToOne
//    @JoinColumn(name="cliente_id", nullable = false)
//	private Person cliente;
	
}
