package br.com.artix.model.performance;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import br.com.artix.model.subcategory.Subcategory;
import br.com.artix.model.util.AbstractEntity;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "performance")
public class Performance extends AbstractEntity {

	@NotBlank(message = "nome é obrigatório!")
	private String name;
	
	@Column(name = "default_description")
	private String defaultDescription;
	
	@ManyToOne
	@JoinColumn(name="subcategory_id", nullable = false)
	private Subcategory subcategory;

}
