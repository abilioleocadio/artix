package br.com.artix.util;

public class Cript {

	public static String encripta(String str) {
		return encripta3(str, 5050);
	}

	public static String decripta(String str) {
		return decripta3(str, 5050);
	}

	private static String encripta3(String str, int chave) {
		final int C1 = 12454;
		final int C2 = 44567;
		final int Key = chave;

		int i = 0;
		int word;
		int cKey = Key;
		int btecKey;
		String shex;

		String sResult = "";

		byte[] senhaBte = new byte[str.length()];
		char senhaArr[] = str.toCharArray();

		for (i = 0; i <= senhaArr.length - 1; i++) {

			senhaBte[i] = (byte) senhaArr[i];
			word = (int) (senhaBte[i] ^ (cKey >> 8));

			shex = Integer.toHexString(word).toUpperCase();

			if (shex.length() == 1) {
				shex = "0" + shex;
			}

			sResult = sResult + shex;

			btecKey = word;
			btecKey = btecKey + cKey;
			btecKey = convertKey(btecKey);

			cKey = (btecKey * C1);
			cKey = convertKey(cKey);
			cKey = (cKey + C2);
			cKey = convertKey(cKey);

		}
		return sResult;
	}

	public static int convertKey(int Key) {

		while (Key > 65535) {
			Key = Key - 65536;
		}
		return Key;

	}

	private static String decripta3(String senha, int chave) {
		final int C1 = 12454;
		final int C2 = 44567;
		int Key = chave;

		int i = 0;
		int word;
		int cKey = Key;
		int b;

		String sResult = "";
		char senhaArr[] = senha.toCharArray();

		while (i < senhaArr.length - 1) {
			String sHex = senha.substring(i, i + 2);
			b = Integer.parseInt(sHex, 16);

			word = (int) (b ^ (cKey >> 8));

			sResult = sResult + ((char) word);

			cKey = b + cKey;
			cKey = (cKey * C1);
			cKey = convertKey(cKey);
			cKey = (cKey + C2);
			cKey = convertKey(cKey);

			i = i + 2;
		}
		return sResult;
	}

	public static void main(String[] args) {
		System.out.println(Cript.encripta("abilio"));
		System.out.println(Cript.encripta("cliente"));
		System.out.println(Cript.encripta("teste"));
		System.out.println(Cript.decripta("4793189B51DF1B83"));
		System.out.println(Cript.decripta("40C73F923BA9E1"));	
		
	}

	
}
