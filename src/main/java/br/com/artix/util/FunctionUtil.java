package br.com.artix.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;

public class FunctionUtil {

	public static boolean isEmpty(String texto) {
		return (texto == null || texto.trim().equals(""));
	}

	public static boolean isEmpty(Collection<?> list) {
		return (list == null || list.isEmpty() || list.size() == 0);
	}

	public static boolean isEmpty(Object obj) {
		return (obj == null || obj.equals(null));
	}
	
	public static boolean isEmpty(Number numero) {
		if (numero == null) {
			return true;
		}

		if (numero instanceof Integer && numero.intValue() <= 0) {
			return true;
		}

		if (numero instanceof Long && numero.longValue() <= 0) {
			return true;
		}

		if (numero instanceof BigDecimal && ((BigDecimal) numero).equals(BigDecimal.ZERO)) {
			return true;
		}

		if (numero instanceof BigInteger && ((BigInteger) numero).equals(BigInteger.ZERO)) {
			return true;
		}

		return false;
	}
	
	public static String removerMascaraTexto(String texto) {
        if (isEmpty(texto)) {
            return texto;
        }
        return texto.replaceAll("[^0-9a-zA-Z]", "");
    }

}
