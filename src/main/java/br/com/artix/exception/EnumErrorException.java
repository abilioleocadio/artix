package br.com.artix.exception;

import org.springframework.http.HttpStatus;

import lombok.Getter;

@Getter
public enum EnumErrorException {

	PARAMETROS_INVALIDOS(HttpStatus.CONFLICT,
			"Erro", 
			"Informações inválidas",
			"O corpo e/ou parametros da requisição inválidos"),
	
	ERROR_SAVE_PERSON(HttpStatus.BAD_REQUEST,
			"Erro", 
			"Erro ao salvar",
			"Erro ao salvar a pessoa no banco!"), 
	
	ERROR_SAVE(HttpStatus.BAD_REQUEST,
			"Erro",
			"Erro ao salvar",
			"Erro ao salvar o registro no banco!"),
	
	ERROR_DELETE(HttpStatus.BAD_REQUEST,
			"Erro",
			"Erro ao excluir",
			"Erro ao excluir o registro no banco!");

	EnumErrorException(HttpStatus httpStatus, String tipo, String nome, String detalhe) {
		this.httpStatus = httpStatus;
		this.tipo = tipo;
		this.nome = nome;
		this.detalhe = detalhe;
	}

	private HttpStatus httpStatus;

	private String nome;

	private String detalhe;

	private String tipo;

}
